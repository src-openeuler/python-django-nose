%global _empty_manifest_terminate_build 0
Name:		python-django-nose
Version:	1.4.7
Release:	2
Summary:	Makes your Django tests simple and snappy
License:	BSD
URL:		http://github.com/jazzband/django-nose
Source0:	https://files.pythonhosted.org/packages/4c/d6/a340da9854cf0a2b54e23cf9147911b1e15a831911428983dd0158572ce9/django-nose-1.4.7.tar.gz
BuildArch:	noarch

Requires:	python3-nose

%description
Makes your Django tests simple and snappy

%package -n python3-django-nose
Summary:	Makes your Django tests simple and snappy
Provides:	python-django-nose
BuildRequires:	python3-devel
BuildRequires:	zlib >= 1.2.11
BuildRequires:	python3-setuptools
%description -n python3-django-nose
Makes your Django tests simple and snappy

%package help
Summary:	Development documents and examples for django-nose
Provides:	python3-django-nose-doc
%description help
Makes your Django tests simple and snappy

%prep
%autosetup -n django-nose-1.4.7

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-django-nose -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sun Apr 24 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.4.7-2
- Add zlib to buildrequires 

* Fri Dec 10 2021 Python_Bot <Python_Bot@openeuler.org> - 1.4.7-1
- Package Init
